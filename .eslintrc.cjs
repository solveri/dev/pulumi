module.exports = {
  root: true,
  env: { node: true },
  plugins: ["@typescript-eslint"],
  parser: "@typescript-eslint/parser",
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/strict-type-checked",
    "plugin:@typescript-eslint/stylistic-type-checked",
  ],
  rules: {
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": "error",
  },
  parserOptions: {
    ecmaVersion: "latest",
    project: true,
    tsconfigRootDir: "__dirname",
  },
  ignorePatterns: ["example/**/*", "nix/**/*"],
}
