import { Input, Output, Config as PulumiConfig, interpolate } from "@pulumi/pulumi"

export interface KubeConfigArguments {
  namespace: string
  serverIp?: Input<string>
  serverPort?: Input<string>
  certificateAuthorityData?: Input<string>
  userToken?: Input<string>
  clusterName?: Input<string>
  contextName?: Input<string>
}

export class Config extends PulumiConfig {
  requireKubeConfig (args: KubeConfigArguments): Output<string> {
    args.serverIp ??= this.requireSecret("kube-config-server-ip")
    args.serverPort ??= "6443"
    args.certificateAuthorityData ??= this.requireSecret("kube-config-certificate-authority-data")
    args.userToken ??= this.requireSecret("kube-config-user-token")
    args.clusterName ??= "solveri"
    args.contextName ??= "solveri"

    return interpolate`
apiVersion: v1
kind: Config
preferences: {}

clusters:
- cluster:
    certificate-authority-data: "${args.certificateAuthorityData}"
    server: "https://${args.serverIp}:${args.serverPort}"
  name: "${args.clusterName}"

users:
- name: service-account
  user:
    as-user-extra: {}
    token: "${args.userToken}"

contexts:
- context:
    cluster: "${args.clusterName}"
    namespace: "${args.namespace}"
    user: service-account
  name: "${args.contextName}"
current-context: "${args.contextName}"
        `
  }
}
