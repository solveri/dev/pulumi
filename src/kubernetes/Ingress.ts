/* eslint-disable @typescript-eslint/naming-convention */
import {
  Ingress as KubeIngress,
  IngressArgs as KubeIngressArgs,
} from "@pulumi/kubernetes/networking/v1"
import { CustomResourceOptions, Input } from "@pulumi/pulumi"
import { Service } from "./Service"

interface IngressArgs {
  host: Input<string>
  service: Service
  namespace?: Input<string>
  ingressClass?: Input<string>
  clusterIssuer?: Input<string>
  secretName?: Input<string>
}

/**
 * A simplified Ingress
 * for exposing a Service port behind a host name.
 */
export class Ingress extends KubeIngress {
  constructor(name: string, args: IngressArgs, opts?: CustomResourceOptions) {
    const namespace = args.namespace ?? name
    const ingressClass = args.ingressClass ?? "nginx"
    const clusterIssuer = args.clusterIssuer ?? "letsencrypt-prod"
    const secretName = args.secretName ?? `${name}-tls`

    const ingressArgs: KubeIngressArgs = {
      metadata: {
        name,
        namespace,
        annotations: {
          "kubernetes.io/ingress.class": ingressClass,
          "cert-manager.io/cluster-issuer": clusterIssuer,
        },
      },
      spec: {
        tls: [
          {
            hosts: [args.host],
            secretName: secretName,
          },
        ],
        rules: [
          {
            host: args.host,
            http: {
              paths: [
                {
                  path: "/",
                  pathType: "Prefix",
                  backend: {
                    service: {
                      name: args.service.name,
                      port: { number: args.service.port },
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    }

    super(name, ingressArgs, opts)
  }
}
