import { Output, interpolate } from "@pulumi/pulumi"
import { Service as KubeService } from "@pulumi/kubernetes/core/v1"

/**
 * A simplified Service,
 * for identifying a Service name and port only
 */
export interface Service {
  name: Output<string>
  namespace: Output<string>
  port: Output<number>

  localClusterHost: Output<string>
}

/**
 * Copies the Service name
 * and the first port number that can be found in spec.
 *
 * The implementation is very simple, and likely error prone.
 * Use with caution.
 *
 * @alpha
 */
export class CopiedService implements Service {
  name: Output<string>
  namespace: Output<string>
  port: Output<number>
  localClusterHost: Output<string>

  constructor(svc: KubeService) {
    this.name = svc.metadata.name
    this.namespace = svc.metadata.namespace
    this.port = svc.spec.ports[0].port

    this.localClusterHost = interpolate`${this.name}.${this.namespace}.svc.cluster.local`
  }
}
