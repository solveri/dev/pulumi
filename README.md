# @solveri/pulumi

## Usage

Make sure you are pointing to the correct registry, for `@solveri` packages.

```shell
echo @solveri:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
```

Install the package.

```shell
npm install @solveri/pulumi
```
